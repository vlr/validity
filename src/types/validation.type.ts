import { ValueType } from "./value.type";
import { Message } from "./message.type";

export interface ItemValidation<T> {
  _value: T;
  _valid: boolean;
  _show: boolean;
  _messages?: Message[];
  _required?: boolean;
  _maxLength?: number;
}

export interface ArrayValidation<T extends Array<any>> extends ItemValidation<T> {
  _items: Validation<T[0]>[];
}

export type ObjectValidation<T> = {
  [P in keyof T]?: Validation<T[P]>;
} & ItemValidation<T>;


export type Validation<T> = T extends Array<any>
  ? ArrayValidation<T>
  : (T extends ValueType
    ? ItemValidation<T>
    : ObjectValidation<T>);
