export type Message = {
  message: string;
  inherited: boolean;
  parameters: any[];
};
