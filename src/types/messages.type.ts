export const messages = {
  invalid: "validation.invalid",
  greaterThan: "validation.greaterThan",
  lessThan: "validation.lessThan",
  equalOrGreater: "validation.equalOrGreater",
  equalOrLess: "validation.equalOrLess",
  required: "validation.required",
  regex: "validation.regex",
  maxLength: "validation.maxLength",
  minLength: "validation.minLength",
  exactLength: "validation.exactLength",
  numeric: "validation.numeric",
  alpha: "validation.alpha",
  alphanumeric: "validation.alphanumeric",
  number: "validation.number",
  numberSize: "validation.numberSize",
  numberPrecision: "validation.numberPrecision",
  oneOf: "validation.oneOf"
};
