import { ItemValidation } from "./validation.type";

export type ValidatorFunc<T> = { (_value: T, _show: boolean): ItemValidation<T>; };

export type ObjectValidator<T> = { [P in keyof T]?: ChildValidators<T[P], T> };

export type Validator<T> = T extends boolean
  ? ValidatorFunc<boolean>
  : (T extends string | number
    ? ValidatorFunc<T>
    : ObjectValidator<T> | ValidatorFunc<T>);

export type Validators<T> = Validator<T> | Validator<T>[];

export type ChildValidatorFunc<T, TParent> = { (_value: T, _show: boolean, parent: TParent): ItemValidation<T>; };

export type ChildValidator<T, TParent> = T extends boolean
  ? ChildValidatorFunc<boolean, TParent> | ValidatorFunc<boolean>
  : (T extends string | number
    ? ChildValidatorFunc<T, TParent> | ValidatorFunc<T>
    : ObjectValidator<T> | ChildValidatorFunc<T, TParent> | ValidatorFunc<T>);

export type ChildValidators<T, TParent> = ChildValidator<T, TParent> | ChildValidator<T, TParent>[];
