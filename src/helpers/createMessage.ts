import { Message } from "../types";

export function createMessage(message: string, ...parameters: any[]): Message {
  return {
    message,
    parameters,
    inherited: false
  };
}
