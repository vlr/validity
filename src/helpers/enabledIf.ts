import { Predicate, ValidatorFunc, ChildPredicate, ChildValidatorFunc } from "../types";
import { DisableValidation } from "../filterValidations";

export function enabledIf<T>(predicate: Predicate<T>): ValidatorFunc<T>;
export function enabledIf<T, TP>(predicate: ChildPredicate<T, TP>): ChildValidatorFunc<T, TP>;
export function enabledIf<T, TP>(predicate: ChildPredicate<T, TP>): ChildValidatorFunc<T, TP> {
  return function (_value: T, _show: boolean, parent: TP): DisableValidation<T> {
    const enabled = predicate(_value, parent);
    return enabled
      ? { _value, _valid: true, _show, disabled: false }
      : { _value, _valid: true, _show, disabled: true };
  };
}
