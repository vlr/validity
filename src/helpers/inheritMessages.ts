import { Message } from "../types";

export function inheritMessages(messages: Message[]): Message[] {
  if (messages == null) { return []; }
  return messages.map(message => ({ ...message, inherited: true }));
}
