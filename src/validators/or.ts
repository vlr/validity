import { validator } from "../helpers";
import { ChildValidatorFunc, ChildValidators, messages, ValidatorFunc, Validators } from "../types";
import { validate } from "../validate";
import { createMessage } from "../helpers/createMessage";

export function or<T>(val1: Validators<T>, val2: Validators<T>, msg?: string): ValidatorFunc<T>;
export function or<T, TP>(val1: ChildValidators<T, TP>, val2: ChildValidators<T, TP>, msg?: string): ChildValidatorFunc<T, TP>;
export function or<T, TP>(val1: ChildValidators<T, TP>, val2: ChildValidators<T, TP>, msg?: string): ChildValidatorFunc<T, TP> {
  const message =  createMessage(msg || messages.invalid);
  return validator((_value: T, parent: TP) => {
    return validate(val1, _value, true, parent)._valid
      || validate(val2, _value, true, parent)._valid;
  }, message);
}
