import { ValidatorFunc, messages } from "../types";
import { createMessage, maxLengthValidator, validator } from "../helpers";
import { isArray } from "util";

export function arrayMaxLength<T>(_maxLength: number, message?: string): ValidatorFunc<T[]> {
  const msg = createMessage(message || messages.maxLength, _maxLength);
  return maxLengthValidator<T[]>(_value => !isArray(_value) || _value.length <= _maxLength, msg, _maxLength);
}

export function arrayMinLength<T>(_minLength: number, message?: string): ValidatorFunc<T[]> {
  const msg = createMessage(message || messages.maxLength, _minLength);
  return validator<T[]>(_value => !isArray(_value) || _value.length >= _minLength, msg);
}
