import { ValidatorFunc, messages } from "../types";
import { validator } from "../helpers";
import { contains } from "@vlr/array-tools";
import { createMessage } from "../helpers/createMessage";

export function oneOf<T>(items: T[], message?: string): ValidatorFunc<T> {
  const msg = createMessage(message || messages.oneOf, items.join(", "));
  return validator(_value => contains(items, _value), msg);
}
