import { validator } from "../helpers";
import { Message, messages, ValidatorFunc } from "../types";
import { createMessage } from "../helpers/createMessage";

/**
 * deprecated
 * @param limit
 * @param message
 */
export function greaterThan(limit: number, message?: string): ValidatorFunc<number> {
  const msg: Message = createMessage(message || messages.greaterThan, limit);
  return validator(value => value > limit, msg);
}

/**
 * deprecated
 * @param limit
 * @param message
 */
export function lessThan(limit: number, message?: string): ValidatorFunc<number> {
  const msg: Message = createMessage(message || messages.lessThan, limit);
  return validator(value => value < limit, msg);
}

/**
 * deprecated
 * @param limit
 * @param message
 */
export function equalOrGreater(limit: number, message?: string): ValidatorFunc<number> {
  const msg: Message = createMessage(message || messages.equalOrGreater, limit);
  return validator(value => value >= limit, msg);
}

/**
 * deprecated
 * @param limit
 * @param message
 */
export function equalOrLess(limit: number, message?: string): ValidatorFunc<number> {
  const msg: Message = createMessage(message || messages.equalOrLess, limit);
  return validator(value => value <= limit, msg);
}
