import { all, flatMap } from "@vlr/array-tools";
import { ArrayValidation, ChildValidator, ChildValidators } from "../types";
import { validate } from "../validate";
import { inheritMessages } from "../helpers/inheritMessages";

export function arrayValidator<T>(validator: ChildValidator<T, T[]>): (_value: T[], _show: boolean) => ArrayValidation<T[]>;
export function arrayValidator<T>(validator: ChildValidator<T, T[]>[]): (_value: T[], _show: boolean) => ArrayValidation<T[]>;
export function arrayValidator<T>(validator: ChildValidators<T, T[]>): (_value: T[], _show: boolean) => ArrayValidation<T[]>;
export function arrayValidator<T>(validator: ChildValidators<T, T[]>): (_value: T[], _show: boolean) => ArrayValidation<T[]> {
  return function (_value: T[], _show: boolean): ArrayValidation<T[]> {
    if (!Array.isArray(_value)) { return <any>{ _valid: true, _show }; }
    const _items = _value.map(item => validate(<ChildValidator<T, T[]>>validator, item, _show, _value));
    const _valid = all(_items, (item: any) => item._valid);
    const messages = flatMap(_items, item => item._messages);
    return { _value, _items, _valid, _show, _messages: inheritMessages(messages) };
  };
}
