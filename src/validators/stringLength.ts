import { isString } from "@vlr/object-tools";
import { maxLengthValidator, validator } from "../helpers";
import { ValidatorFunc, messages } from "../types";
import { createMessage } from "../helpers/createMessage";

export function maxLength(_maxLength: number, message?: string): ValidatorFunc<string> {
  const msg = createMessage(message || messages.maxLength, _maxLength);
  return maxLengthValidator<string>(_value => !isString(_value) || _value.length <= _maxLength, msg, _maxLength);
}

export function minLength(length: number, message?: string): ValidatorFunc<string> {
  const msg = createMessage(message || messages.minLength, length);
  return validator<string>(_value => !isString(_value) || _value.length >= length, msg);
}

export function exactLength(length: number, message?: string): ValidatorFunc<string> {
  const msg = createMessage(message || messages.exactLength, length);
  return maxLengthValidator<string>(_value => !isString(_value) || _value.length === length, msg, length);
}
