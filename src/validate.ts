import { flatMap, all } from "@vlr/array-tools";
import { get, IMap, keys } from "@vlr/map-tools/objectMap";
import { isFunction } from "@vlr/object-tools";
import { Validation } from "./types/validation.type";
import { ChildValidator, ChildValidators, Validator, ObjectValidator, Validators } from "./types/validator.type";
import { Message } from "./types";
import { inheritMessages } from "./helpers/inheritMessages";
import { filterValidations } from "./filterValidations";

export function validate<T>(validator: ObjectValidator<T>, _value: T, _show?: boolean): Validation<T>;
export function validate<T>(validator: Validator<T>, _value: T, _show?: boolean): Validation<T>;
export function validate<T>(validators: Validator<T>[], _value: T, _show?: boolean): Validation<T>;
export function validate<T>(validators: Validators<T>, _value: T, _show?: boolean): Validation<T>;
export function validate<T, TParent>(validator: ChildValidator<T, TParent>, _value: T, _show?: boolean, parent?: TParent): Validation<T>;
export function validate<T, TParent>(validators: ChildValidator<T, TParent>[], _value: T, _show?: boolean, parent?: TParent): Validation<T>;
export function validate<T, TParent>(validators: ChildValidators<T, TParent>, _value: T, _show: boolean, parent?: TParent): Validation<T>;
export function validate<T, TParent>(validators: ChildValidators<T, TParent>, _value: T, _show: boolean = true, parent?: TParent): Validation<T> {
  if (!Array.isArray(validators)) { validators = [validators]; }
  const validations = validators.map(v => runValidator(v, _value, _show, parent));
  const filtered = filterValidations(validations);
  if (!filtered.length) { return <any>{ _valid: true, _show }; }
  return compileResult(validations, _show);
}

export function compileResult<T>(validations: Validation<T>[], _show: boolean): Validation<T> {
  const _valid = all(validations, val => val._valid);
  _show = _show && !_valid;
  const resultObj = _valid
    ? { _valid, _show }
    : { _valid, _show, _messages: flatMap(validations, v => v._messages) };
  const result = Object.assign({}, ...validations, resultObj);
  return result;
}

function runValidator<T, TParent>(validator: ChildValidator<T, TParent>, _value: T, _show: boolean, parent: TParent): Validation<T> {
  return isFunction(validator)
    ? (<any>validator)(_value, _show, parent)
    : objectValidator(<any>validator, _value, _show);
}

function objectValidator<T>(validator: IMap<any>, _value: IMap<any>, _show: boolean): Validation<T> {
  const fields = keys(validator);
  const messages: Message[] = [];
  const result: any = { _value, _show, _valid: true };
  if (_value == null) { return result; }
  for (const field of fields) {
    const v: ChildValidator<any, T> = get(validator, field);
    const fieldValue: any = get(_value, field);
    const fieldValidation = validate(v, fieldValue, _show, _value);
    result._valid = result._valid && fieldValidation._valid;
    result[field] = fieldValidation;
    messages.push(...inheritMessages(fieldValidation._messages));
  }
  result._messages = messages;
  return result;
}
