import { expect } from "chai";

import { customMessage, expectCustom } from "./testSupport";
import { arrayMaxLength, arrayMinLength } from "../src/validators/arrayLength";

describe("arrayMaxLength", function (): void {
  it("should return valid for shorter array", function (): void {
    // arrange

    // act
    const result = arrayMaxLength(3)([1, 2], true);

    // assert
    expect(result._valid).equals(true);
    expect(result._maxLength).equals(3);
  });

  it("should return invalid for longer array", function (): void {
    // arrange

    // act
    const result = arrayMaxLength(2, customMessage)([1, 2, 3], true);

    // assert
    expect(result._valid).equals(false);
    expect(result._show).equals(true);
    expectCustom(result);
  });
});


describe("arrayMinLength", function (): void {
  it("should return valid for array with enough elements", function (): void {
    // arrange

    // act
    const result = arrayMinLength(2)([1, 2], true);

    // assert
    expect(result._valid).equals(true);
  });

  it("should return invalid for shorter array", function (): void {
    // arrange

    // act
    const result = arrayMinLength(2, customMessage)([1], true);

    // assert
    expect(result._valid).equals(false);
    expect(result._show).equals(true);
    expectCustom(result);
  });
});
