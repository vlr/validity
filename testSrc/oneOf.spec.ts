import { oneOf } from "../src/validators/oneOf";
import { expect } from "chai";
import { customMessage, expectCustom } from "./testSupport";

describe("oneOf", function (): void {
  it("should return valid for included element", function (): void {
    // arrange
    const items = [1, 2, 3];

    // act
    const result = oneOf(items)(2, true);

    // assert
    expect(result._valid).equals(true);
  });

  it("should return invalid for excluded element", function (): void {
    // arrange
    const items = [1, 2, 3];

    // act
    const result = oneOf(items, customMessage)(4, true);

    // assert
    expect(result._valid).equals(false);
    expect(result._show).equals(true);
    expectCustom(result);
  });

  it("should set show flag", function (): void {
    // arrange
    const items = [1, 2, 3];

    // act
    const result = oneOf(items)(4, false);

    // assert
    expect(result._show).equals(false);
  });
});
