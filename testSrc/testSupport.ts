import { expect } from "chai";
import { ItemValidation, ValidatorFunc } from "../src/types";

export const validNum: ValidatorFunc<number> = () => ({ _value: 1, _valid: true, _show: true });
export const invalidNum: ValidatorFunc<number> = () => ({ _value: 1, _valid: false, _show: true });

export const validBool: ValidatorFunc<boolean> = (_value: boolean) => ({ _value, _valid: true, _show: true });

export const customMessage: string = "customMessage";

export function expectMessage<T>(validation: ItemValidation<T>, message: string): void {
  expect(validation._messages[0] != null).equals(true);
  expect(validation._messages[0].message).equals(message);
}

export function expectCustom<T>(validation: ItemValidation<T>): void {
  expectMessage(validation, customMessage);
}
