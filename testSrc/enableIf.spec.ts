import { required, validate } from "../src";
import { enabledIf } from "../src/helpers/enabledIf";
import { expect } from "chai";

describe("enabledIf", function (): void {
  it("should disable validations after it", function (): void {
    // arrange
    const validators = [enabledIf(() => false), required()];
    // act
    const result = validate(validators, null);

    // assert
    expect(result._valid).equals(true);
    expect(!!result._required).equals(false);
  });

  it("should not disable validations before it", function (): void {
    // arrange
    const validators = [required(), enabledIf(() => false)];
    // act
    const result = validate(validators, null);

    // assert
    expect(result._valid).equals(false);
    expect(result._required).equals(true);
  });
});
