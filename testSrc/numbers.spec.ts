import { expect } from "chai";
import { greaterThan, equalOrGreater, lessThan, equalOrLess } from "../src/validators/numbers";
import { expectMessage, customMessage, expectCustom } from "./testSupport";
import { messages } from "../src";

describe("moreThan", function (): void {
  it("should return valid for null", function (): void {
    // arrange

    // act
    const result = greaterThan(5)(null, true);

    // assert
    expect(result._valid).equals(true);
  });

  it("should return valid for higher number", function (): void {
    // arrange

    // act
    const result = greaterThan(5)(6, true);

    // assert
    expect(result._valid).equals(true);
  });

  it("should return invalid for equal number", function (): void {
    // arrange

    // act
    const result = greaterThan(5)(5, true);

    // assert
    expect(result._valid).equals(false);
  });

  it("should return invalid for lower number", function (): void {
    // arrange

    // act
    const result = greaterThan(5)(4, true);

    // assert
    expect(result._valid).equals(false);
    expect(result._show).equals(true);
    expectMessage(result, messages.greaterThan);
  });

  it("should set customMessage", function (): void {
    // arrange

    // act
    const result = greaterThan(5, customMessage)(4, false);

    // assert
    expect(result._show).equals(false);
    expectCustom(result);
  });
});


describe("lessThan", function (): void {
  it("should return invalid for higher number", function (): void {
    // arrange

    // act
    const result = lessThan(5)(6, true);

    // assert
    expect(result._valid).equals(false);
  });

  it("should return invalid for equal number", function (): void {
    // arrange

    // act
    const result = lessThan(5)(5, true);

    // assert
    expect(result._valid).equals(false);
    expectMessage(result, messages.lessThan);
    expect(result._show).equals(true);
  });

  it("should set customMessage", function (): void {
    // arrange

    // act
    const result = lessThan(5, customMessage)(5, false);

    // assert
    expectCustom(result);
    expect(result._show).equals(false);
  });

  it("should return valid for lower number", function (): void {
    // arrange

    // act
    const result = lessThan(5)(4, true);

    // assert
    expect(result._valid).equals(true);
  });
});



describe("equalOrMore", function (): void {
  it("should return valid for higher number", function (): void {
    // arrange

    // act
    const result = equalOrGreater(5)(6, true);

    // assert
    expect(result._valid).equals(true);
  });

  it("should return valid for equal number", function (): void {
    // arrange

    // act
    const result = equalOrGreater(5)(5, true);

    // assert
    expect(result._valid).equals(true);
  });

  it("should return invalid for lower number", function (): void {
    // arrange

    // act
    const result = equalOrGreater(5, customMessage)(4, true);

    // assert
    expect(result._valid).equals(false);
    expectCustom(result);
  });
});

describe("equalOrLess", function (): void {
  it("should return invalid for higher number", function (): void {
    // arrange

    // act
    const result = equalOrLess(5, customMessage)(6, true);

    // assert
    expect(result._valid).equals(false);
    expectCustom(result);
  });

  it("should return valid for equal number", function (): void {
    // arrange

    // act
    const result = equalOrLess(5)(5, true);

    // assert
    expect(result._valid).equals(true);
  });

  it("should return valid for lower number", function (): void {
    // arrange

    // act
    const result = equalOrLess(5)(4, true);

    // assert
    expect(result._valid).equals(true);
  });
});

