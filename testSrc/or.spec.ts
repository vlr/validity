import { expect } from "chai";
import { or } from "../src/validators/or";
import { invalidNum, validNum, customMessage, expectCustom } from "./testSupport";

describe("or", function (): void {
  it("should return valid if one is valid", function (): void {
    // arrange

    // act
    const result1 = or(invalidNum, validNum)(1, true);
    const result2 = or(validNum, validNum)(1, true);

    // assert
    expect(result1._valid).equals(true);
    expect(result2._valid).equals(true);
  });

  it("should return invalid if both are invalid", function (): void {
    // arrange

    // act
    const result = or(invalidNum, invalidNum, customMessage)(1, true);

    // assert
    expect(result._valid).equals(false);
    expectCustom(result);
  });
});
