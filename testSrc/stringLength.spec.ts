import { expect } from "chai";
import { maxLength, minLength, exactLength } from "../src/validators/stringLength";
import { customMessage, expectCustom } from "./testSupport";

describe("maxLength", function (): void {
  it("should return valid for shorter string", function (): void {
    // arrange

    // act
    const result = maxLength(5)("abhy", true);

    // assert
    expect(result._valid).equals(true);
    expect(result._maxLength).equals(5);
  });

  it("should return invalid for longer string", function (): void {
    // arrange

    // act
    const result = maxLength(5, customMessage)("asdf2213", true);

    // assert
    expect(result._valid).equals(false);
    expect(result._show).equals(true);
    expectCustom(result);
  });

  it("should set show flag", function (): void {
    // arrange

    // act
    const result = maxLength(5)("asdf2213", false);

    // assert
    expect(result._show).equals(false);
  });
});


describe("minLength", function (): void {
  it("should return invalid for shorter string", function (): void {
    // arrange

    // act
    const result = minLength(5, customMessage)("abhy", true);

    // assert
    expect(result._valid).equals(false);
    expectCustom(result);
  });

  it("should return valid for longer string", function (): void {
    // arrange

    // act
    const result = minLength(5)("asdf2213", true);

    // assert
    expect(result._valid).equals(true);
  });
});


describe("exactLength", function (): void {
  it("should return invalid for shorter string", function (): void {
    // arrange

    // act
    const result = exactLength(5, customMessage)("abhy", true);

    // assert
    expect(result._valid).equals(false);
    expect(result._maxLength).equals(5);
    expectCustom(result);
  });

  it("should return invalid for longer string", function (): void {
    // arrange

    // act
    const result = exactLength(5)("abhy123", true);

    // assert
    expect(result._valid).equals(false);
  });

  it("should return valid for string of length", function (): void {
    // arrange

    // act
    const result = exactLength(5)("asdf2", true);

    // assert
    expect(result._valid).equals(true);
  });
});
